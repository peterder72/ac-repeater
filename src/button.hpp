#pragma once

#include <Arduino.h>

#define DBNC 15

class Button {
    int pin;
    bool state;

    uint32_t timer;

    public:

    Button(int pin);
    bool pressed();
    
};