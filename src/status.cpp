#include "status.hpp"

Status::Status(int pin) : cur_state(IDLE), timer(0), led_pin(pin), led_state(0), counter(0)
{
    pinMode(led_pin, OUTPUT);
}

void Status::write(bool val)
{
    led_state = val;
    digitalWrite(led_pin, val);
}

void Status::update()
{
    switch (cur_state)
    {
    case IDLE:
        write(0);
        break;
    case PLAYING:
        if (timer == 0)
        {
            write(1);
            timer = millis();
        }
        else if (millis() - timer > PLAY_PULSE)
        {
            timer = 0;
            cur_state = IDLE;
        }
        break;
    case RECORDED:
        if (timer == 0)
        {
            timer = millis();
        }
        else if (millis() - timer > RECORDED_PULSE)
        {
            timer = millis();
            write(!led_state);
            if (++counter >= RECORDED_NTIMES)
            {
                cur_state = IDLE;
                counter = 0;
            }
        }
    case RECORDING:
        if (timer == 0)
        {
            timer = millis();
        }
        else if (millis() - timer > BLINK_PULSE)
        {
            timer = 0;
            write(!led_state);
        }
        break;
    case ERROR:
        if (timer == 0)
        {
            timer = millis();
        }
        else if (millis() - timer > ((led_state) ? ERROR_ON_PULSE: ERROR_OFF_PULSE))
        {
            timer = 0;
            write(!led_state);
        }
        break;
    }
}

void Status::recording(bool val)
{
    timer = 0;
    cur_state = (val) ? RECORDING : RECORDED;
}

void Status::error()
{
    timer = 0;
    cur_state = ERROR;
}

void Status::playing(bool val)
{
    timer = 0;
    cur_state = (val) ? PLAYING : IDLE;
}

bool Status::is_recording()
{
    return cur_state == RECORDING;
}